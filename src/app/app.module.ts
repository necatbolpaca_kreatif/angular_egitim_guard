import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { SiparisComponent } from './component/siparis/siparis.component';
import { LoginGuard } from './util/login-guard';
import { SubComponentComponent } from './component/sub-component/sub-component.component';
import { SubComponent } from './component/sub/sub.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SiparisComponent,
    SubComponentComponent,
    SubComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
