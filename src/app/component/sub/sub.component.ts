import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sub',
  templateUrl: './sub.component.html',
  styleUrls: ['./sub.component.css']
})
export class SubComponent implements OnInit {

  @Input() bankList: string;
  @Input("label-tag") labelTag: string;
  public model: Array<string>;
  constructor() { 
  }
  
  ngOnInit() {
    this.model = this.bankList.split(',');
  }

}
