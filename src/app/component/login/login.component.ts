import { Component, OnInit } from '@angular/core';
import { LoginModel } from './login.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public model: LoginModel;

  constructor(private router: Router) {
    this.model = new LoginModel();
  }

  ngOnInit() {
  }

  public CheckLogin(): void {
    if(this.model.userName === 'dnm' && this.model.password === 'dnm'){
      localStorage.setItem("username", this.model.userName);
      localStorage.setItem("password", this.model.password);
      this.router.navigate(["siparis"]);
    } else {
      alert('Hatalı giriş');
    }
  }

}
