export class LoginModel
{
    public userName: string;
    public password: string;

    constructor() {
        this.userName = '';
        this.password = '';
    }
}