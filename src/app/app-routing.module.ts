import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiparisComponent } from './component/siparis/siparis.component';
import { LoginComponent } from './component/login/login.component';
import { LoginGuard } from './util/login-guard';

const routes: Routes = [
  {path: "siparis", component: SiparisComponent, canActivate: [LoginGuard]},
  {path: "login", component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
